//
//  User.swift
//  BuildUp
//
//  Created by Nikita Elizarov on 04.05.2020.
//  Copyright © 2020 Nikita Elizarov. All rights reserved.
//

import Foundation

// MARK: - User
struct User: Codable {
    let dailyFreeTime: Int
    let userName, uuid: String
    let weeklyFreeTime: Int

    enum CodingKeys: String, CodingKey {
        case dailyFreeTime
        case userName = "user_name"
        case uuid, weeklyFreeTime
    }
}

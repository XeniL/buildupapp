//
//  AuthViewController.swift
//  BuildUp
//
//  Created by Nikita Elizarov on 03.05.2020.
//  Copyright © 2020 Nikita Elizarov. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField

protocol AuthDelegate {
    func completeRegistration(daysFree: Int, timeSpent: Int)
}

class AuthViewController: UIViewController, AuthDelegate {
    
    let buildUpNetworker = BuildUpNetworking()
    
    @IBOutlet weak private var userNameTextField: SkyFloatingLabelTextField!
    @IBOutlet weak private var submitButton: ShadowButton!
        
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let gradient = CAGradientLayer()
        let bounds = UIScreen.main.bounds
        let topColor = #colorLiteral(red: 0, green: 0.4784313725, blue: 0.8823529412, alpha: 1)
        let bottomColor = #colorLiteral(red: 0.3529411765, green: 0.7843137255, blue: 0.9803921569, alpha: 1)
        gradient.frame = bounds
        gradient.colors = [topColor.cgColor, bottomColor.cgColor]
        gradient.locations = [ 0.0, 1.0]
        
        self.view.layer.insertSublayer(gradient, at: 0)
            
        applyGradientToNavBar()
        userNameTextField.delegate = self
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    @IBAction func proceed(_ sender: UIButton) {
        completeLogin()
    }
    
    func completeLogin() {
        
        buildUpNetworker.login(userName: userNameTextField.text ?? "") { result in
            switch result {
            case .success(let response):
                do {
                    let filteredResponse = try response.filterSuccessfulStatusCodes()
                    let user = try filteredResponse.map(User.self)
                    AppSettings.instance.userName = user.userName
                    AppSettings.instance.uuid = user.uuid
                    AppSettings.instance.dailyFreeTime = user.dailyFreeTime
                    AppSettings.instance.weeklyFreeTime = user.weeklyFreeTime

                    let vc: ProfessionViewController = ProfessionViewController.fromStoryboard()
                    let navController = UINavigationController(rootViewController: vc)

                    navController.modalPresentationStyle = .fullScreen
                    self.present(navController, animated: true)
                }
                catch {
                    let vc: UserInfoViewController = UserInfoViewController.fromStoryboard()
                    vc.userName = self.userNameTextField.text ?? ""
                    vc.authDelegate = self
                    self.present(vc, animated: true)
                    
                }

            case .failure:
                return
            }
        }
    }
    
    func completeRegistration(daysFree: Int, timeSpent: Int) {
        let user = User(dailyFreeTime: daysFree, userName: userNameTextField.text ?? "", uuid: UUID.init().uuidString, weeklyFreeTime: timeSpent)
        
        buildUpNetworker.register(userName: user.userName,
                                  UUID: user.uuid,
                                  dailyFreeTime: user.weeklyFreeTime,
                                  weeklyFreeTime: user.dailyFreeTime, completion: { result in
                                    switch result {
                                    case .success:
                                        self.completeLogin()
                                    case .failure(let error):
                                        print(error.errorDescription ?? "")
                                    }
        })
        
        let vc: ProfessionViewController = ProfessionViewController.fromStoryboard()
        let navController = UINavigationController(rootViewController: vc)

        navController.modalPresentationStyle = .fullScreen
        navController.navigationController?.navigationBar.isOpaque = true
        self.present(navController, animated: true)
     }
}

extension AuthViewController: UITextFieldDelegate {
    
}

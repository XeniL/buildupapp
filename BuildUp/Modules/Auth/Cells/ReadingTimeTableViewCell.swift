//
//  ReadingTimeTableViewCell.swift
//  BuildUp
//
//  Created by Nikita Elizarov on 03.05.2020.
//  Copyright © 2020 Nikita Elizarov. All rights reserved.
//

import UIKit

class ReadingTimeTableViewCell: UITableViewCell {
    
    @IBOutlet weak var timePicker: UIDatePicker!
    
    var onTimeChosen: ((Date) -> Void)?

    override func awakeFromNib() {
        super.awakeFromNib()
        timePicker.setValue(UIColor.white, forKeyPath: "textColor")
    }

    @IBAction func dateTimeChosen(_ sender: UIDatePicker) {
        onTimeChosen?(sender.date)
    }
}

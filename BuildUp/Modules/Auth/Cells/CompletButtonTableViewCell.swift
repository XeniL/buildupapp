//
//  CompletButtonTableViewCell.swift
//  BuildUp
//
//  Created by Nikita Elizarov on 03.05.2020.
//  Copyright © 2020 Nikita Elizarov. All rights reserved.
//

import UIKit

class CompletButtonTableViewCell: UITableViewCell {
    
    var onCompletion: (() -> Void)?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func onComplete(_ sender: UIButton) {
        onCompletion?()
    }
    
}

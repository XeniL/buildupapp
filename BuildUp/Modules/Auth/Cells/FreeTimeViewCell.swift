//
//  FreeTimeViewCell.swift
//  BuildUp
//
//  Created by Nikita Elizarov on 03.05.2020.
//  Copyright © 2020 Nikita Elizarov. All rights reserved.
//

import UIKit

class FreeTimeViewCell: UITableViewCell {
    
    @IBOutlet weak var segmentController: UISegmentedControl!
    @IBOutlet weak var shadowView: ShadowView!
    
    var onDaysChosen: ((Int) -> ())?
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    @IBAction func segmentedControlPressed(_ sender: UISegmentedControl) {
        onDaysChosen?(sender.selectedSegmentIndex + 1)
    }
}


//
//  UserInfoViewController.swift
//  BuildUp
//
//  Created by Nikita Elizarov on 03.05.2020.
//  Copyright © 2020 Nikita Elizarov. All rights reserved.
//

import UIKit

class UserInfoViewController: UIViewController {
    
    @IBOutlet weak var infoView: ShadowView!
    @IBOutlet weak var greetingsLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    
    var days = 1
    var timeAvailable: Int = 0
    var userName: String = "пользователь"
    
    var authDelegate: AuthDelegate!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        greetingsLabel.text = "Приветствуем тебя, \(userName)!"
        tableView.register(UINib(nibName: String(describing: FreeTimeViewCell.self), bundle: nil), forCellReuseIdentifier: String(describing: FreeTimeViewCell.self))
        tableView.register(UINib(nibName: String(describing: ReadingTimeTableViewCell.self), bundle: nil), forCellReuseIdentifier: String(describing: ReadingTimeTableViewCell.self))
        tableView.register(UINib(nibName: String(describing: CompletButtonTableViewCell.self), bundle: nil), forCellReuseIdentifier: String(describing: CompletButtonTableViewCell.self))
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.reloadData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        UIView.animate(withDuration: 1.5, delay: 0, options: [.autoreverse, .repeat], animations: {[weak self] in
            self?.infoView.transform = CGAffineTransform(scaleX: 1.03, y: 1.03)
        })
    }
}

extension UserInfoViewController: UITableViewDelegate, UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: FreeTimeViewCell.self), for: indexPath) as! FreeTimeViewCell
            cell.onDaysChosen = { days in
                self.days = days
            }
            return cell
        } else if indexPath.row == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: ReadingTimeTableViewCell.self), for: indexPath) as! ReadingTimeTableViewCell
            cell.onTimeChosen = { date in
                let components = Calendar.current.dateComponents([.hour, .minute], from: date)
                let minutes = (components.hour! * 60) + components.minute!
                self.timeAvailable = minutes
            }
            return cell
        } else if indexPath.row == 2 {
            let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: CompletButtonTableViewCell.self), for: indexPath) as! CompletButtonTableViewCell
            cell.onCompletion = {
                self.dismiss(animated: true) {
                    self.authDelegate.completeRegistration(daysFree: self.days, timeSpent: self.timeAvailable)
                }
            }
            return cell
        }
        
        return UITableViewCell()
    }
}

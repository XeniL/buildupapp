//
//  ProfessionViewController.swift
//  BuildUp
//
//  Created by Nikita Elizarov on 03.05.2020.
//  Copyright © 2020 Nikita Elizarov. All rights reserved.
//

import UIKit

class ProfessionViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    var professions: [Profession] = [Profession(name: "Программист", image: UIImage(named: "profProg")!),
                                     Profession(name: "Дизайнер", image: UIImage(named: "profDesign")!),
                                     Profession(name: "Маркетолог", image: UIImage(named: "profMarket")!)]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Профессии"
        navigationItem.largeTitleDisplayMode = .always
        
        tableView.register(UINib(nibName: String(describing: ProfessionTableViewCell.self), bundle: nil), forCellReuseIdentifier: String(describing: ProfessionTableViewCell.self))
                
        tableView.delegate = self
        tableView.dataSource = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        applyGradientToNavBar()
    }
}

extension ProfessionViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return professions.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
         let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: ProfessionTableViewCell.self), for: indexPath) as! ProfessionTableViewCell
        cell.configure(title: professions[indexPath.row].name, image: professions[indexPath.row].image)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = ProfessionListViewController.fromStoryboard()
        vc.title = professions[indexPath.row].name
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

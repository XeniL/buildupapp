//
//  ProfessionTableViewCell.swift
//  BuildUp
//
//  Created by Nikita Elizarov on 03.05.2020.
//  Copyright © 2020 Nikita Elizarov. All rights reserved.
//

import UIKit

class ProfessionTableViewCell: UITableViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var profImage: UIImageView! {
        didSet {
            profImage.layer.shadowOpacity = 0.5
            profImage.layer.shadowRadius = 5
            profImage.layer.shadowColor = UIColor.lightGray.cgColor
            profImage.clipsToBounds = false
            profImage.layer.masksToBounds = false
            profImage.layer.cornerRadius = 12
        }
    }
    @IBOutlet weak var shadowView: UIView!
    
    func configure(title: String, image: UIImage) {
        titleLabel.text = title
        profImage.image = image
    }
    
}

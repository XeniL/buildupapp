//
//  ProfessionDetailViewController.swift
//  BuildUp
//
//  Created by Nikita Elizarov on 03.05.2020.
//  Copyright © 2020 Nikita Elizarov. All rights reserved.
//

import UIKit

class ProfessionDetailViewController: UIViewController {
    
    @IBOutlet weak var button: UIButton!
    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var text: UILabel!
    @IBOutlet weak var containerView: ShadowView! {
        didSet {
            containerView.layer.shadowColor = UIColor.lightGray.cgColor
            containerView.layer.shadowOpacity = 0.1
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func procceedToRoadmap(_ sender: UIButton) {
        let tabBar: TabBarController = TabBarController.fromStoryboard()
        tabBar.selectedIndex = 1
        tabBar.modalPresentationStyle = .fullScreen
        self.present(tabBar, animated: true)
    }
}

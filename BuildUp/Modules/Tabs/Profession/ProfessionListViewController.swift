//
//  ProfessionListViewController.swift
//  BuildUp
//
//  Created by Nikita Elizarov on 04.05.2020.
//  Copyright © 2020 Nikita Elizarov. All rights reserved.
//

import UIKit

class ProfessionListViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    
    var professions = ["Java разработчик", "C# разработчик", "C++ разработчик", "iOS разработчик", "Android разработчик", "Java-script разработчик", "Erlang разработчик", "Data science"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.largeTitleDisplayMode = .never

        tableView.register(UINib(nibName: String(describing: ProfessionListTableViewCell.self), bundle: nil), forCellReuseIdentifier: String(describing: ProfessionListTableViewCell.self))
        tableView.layer.shadowOpacity = 0.5
        tableView.layer.shadowRadius = 5
        tableView.layer.shadowColor = UIColor.lightGray.cgColor
        tableView.clipsToBounds = false
        tableView.layer.masksToBounds = false

        tableView.delegate = self
        tableView.dataSource = self
    }
}

extension ProfessionListViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return professions.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: ProfessionListTableViewCell.self), for: indexPath) as! ProfessionListTableViewCell
        let profession = professions[indexPath.row]
        let isLast = professions.last == profession
        cell.configure(title: professions[indexPath.row], isSeperated: !isLast)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = ProfessionDetailViewController.fromStoryboard()
        vc.title = professions[indexPath.row]
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

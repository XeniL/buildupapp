//
//  ModuleProgressTableViewCell.swift
//  BuildUp
//
//  Created by Nikita Elizarov on 03.05.2020.
//  Copyright © 2020 Nikita Elizarov. All rights reserved.
//

import UIKit

class ModuleProgressTableViewCell: UITableViewCell {

    @IBOutlet weak var progressBar: UIProgressView! {
        didSet {
            progressBar.transform = progressBar.transform.scaledBy(x: 1, y: 16)
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}

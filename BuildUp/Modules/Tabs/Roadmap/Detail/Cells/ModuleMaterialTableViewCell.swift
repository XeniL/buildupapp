//
//  ModuleMaterialTableViewCell.swift
//  BuildUp
//
//  Created by Nikita Elizarov on 03.05.2020.
//  Copyright © 2020 Nikita Elizarov. All rights reserved.
//

import UIKit

class ModuleMaterialTableViewCell: UITableViewCell {
    
    @IBOutlet weak var collectionView: DynamicSizeCollectionView!
    @IBOutlet weak var collectionViewHeight: NSLayoutConstraint!
    
    var onCategorySelected: (([Content])-> Void)?

    var categoryArray: [ContentType] = []
    var contents: [Content] = []
    
    override func awakeFromNib() {
        super.awakeFromNib()
        collectionView.register(UINib(nibName: String(describing: MaterialCollectionViewCell.self), bundle: nil), forCellWithReuseIdentifier: String(describing: MaterialCollectionViewCell.self))
        collectionView.delegate = self
        collectionView.dataSource = self
        
//        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
//        layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
//        layout.itemSize = CGSize(width: 184, height: 184)
//        layout.minimumInteritemSpacing = 0
//        layout.minimumLineSpacing = 0
//        collectionView!.collectionViewLayout = layout
        
        let flow: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        let itemSpacing: CGFloat = 2
           let itemsInOneLine: CGFloat = 3
        flow.sectionInset = UIEdgeInsets(top: 0, left: 16, bottom: 0, right: 16)
        let width = UIScreen.main.bounds.size.width - itemSpacing * CGFloat(itemsInOneLine - 1)
           flow.itemSize = CGSize(width: floor(width/itemsInOneLine), height: width/itemsInOneLine)
           flow.minimumInteritemSpacing = 2
           flow.minimumLineSpacing = 2
        collectionView.collectionViewLayout = flow
        
        layoutSubviews()
    }

    func configure(contents: [Content]) {
        var categories: Set<ContentType> = []
        self.contents = contents
        
        for content in contents {
            categories.insert(ContentType(rawValue: content.contentType)!)
        }
        
        categoryArray = Array(categories)
        collectionView.reloadData()
        layoutSubviews()
    }
    
    override func layoutSubviews() {
        self.collectionViewHeight.constant = self.collectionView.collectionViewLayout.collectionViewContentSize.height
    }
    
}

extension ModuleMaterialTableViewCell: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return categoryArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: MaterialCollectionViewCell.self), for: indexPath) as! MaterialCollectionViewCell
        cell.configure(title: categoryArray[indexPath.row].localized, image: categoryArray[indexPath.row].image)
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        var listContents: [Content] = []
        for content in contents {
            if categoryArray[indexPath.row].rawValue == content.contentType {
                listContents.append(content)
            }
        }
        onCategorySelected?(listContents)
    }
}

//
//  MaterialCollectionViewCell.swift
//  BuildUp
//
//  Created by Nikita Elizarov on 03.05.2020.
//  Copyright © 2020 Nikita Elizarov. All rights reserved.
//

import UIKit

class MaterialCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    
    @IBOutlet weak var shadowView: ShadowView! {
        didSet {
            shadowView.layer.shadowColor = UIColor.lightGray.cgColor
        }
    }
    
    func configure(title: String, image: UIImage) {
        titleLabel.text = title
        imageView.image = image
    }
}

//
//  ModuleDetailViewController.swift
//  BuildUp
//
//  Created by Nikita Elizarov on 03.05.2020.
//  Copyright © 2020 Nikita Elizarov. All rights reserved.
//

import UIKit

class ModuleDetailViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    var contentInfo: RoadElement!
    var contents: [Content]!

    override func viewDidLoad() {
        super.viewDidLoad()
                
        tableView.register(UINib(nibName: String(describing: ModuleProgressTableViewCell.self), bundle: nil), forCellReuseIdentifier: String(describing: ModuleProgressTableViewCell.self))
        
        tableView.register(UINib(nibName: String(describing: ModuleMaterialTableViewCell.self), bundle: nil), forCellReuseIdentifier: String(describing: ModuleMaterialTableViewCell.self))
        
        tableView.register(UINib(nibName: String(describing: ModuleDescriptionTableViewCell.self), bundle: nil), forCellReuseIdentifier: String(describing: ModuleDescriptionTableViewCell.self))
        
        tableView.register(UINib(nibName: String(describing: ModuleButtonTableViewCell.self), bundle: nil), forCellReuseIdentifier: String(describing: ModuleButtonTableViewCell.self))
        
        tableView.delegate = self
        tableView.dataSource = self
        
        tableView.reloadData()
    }
}

extension ModuleDetailViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: ModuleProgressTableViewCell.self), for: indexPath) as! ModuleProgressTableViewCell
            return cell
        } else if indexPath.row == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: ModuleMaterialTableViewCell.self), for: indexPath) as! ModuleMaterialTableViewCell
            cell.configure(contents: contents)
            cell.onCategorySelected = { list in
                let vc: ModuleListViewController = ModuleListViewController.fromStoryboard()
                vc.title = ContentType(rawValue: list[0].contentType)?.localized
                vc.contents = list
                self.navigationController?.pushViewController(vc, animated: true)
            }
            return cell
        } else if indexPath.row == 2 {
            let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: ModuleDescriptionTableViewCell.self), for: indexPath) as! ModuleDescriptionTableViewCell
            cell.configure(title: contentInfo.name, subtitle: contentInfo.roadDescription)
            return cell
        } else if indexPath.row == 3 {
            let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: ModuleButtonTableViewCell.self), for: indexPath) as! ModuleButtonTableViewCell
            return cell
        }
        
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    }
}

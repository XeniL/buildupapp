//
//  ModuleListTableViewCell.swift
//  BuildUp
//
//  Created by Nikita Elizarov on 04.05.2020.
//  Copyright © 2020 Nikita Elizarov. All rights reserved.
//

import UIKit

class ModuleListTableViewCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subtititleLabel:UILabel!
    @IBOutlet weak var seperatorView: UIView!
    
    func configure(title: String, subtitle: String, isSeperated: Bool) {
        titleLabel.text = title
        subtititleLabel.text = subtitle
        seperatorView.isHidden = !isSeperated
    }
    
}

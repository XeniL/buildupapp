//
//  ModuleListViewController.swift
//  BuildUp
//
//  Created by Nikita Elizarov on 04.05.2020.
//  Copyright © 2020 Nikita Elizarov. All rights reserved.
//

import UIKit

class ModuleListViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
        
    var contents: [Content] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.largeTitleDisplayMode = .never

        tableView.register(UINib(nibName: String(describing: ModuleListTableViewCell.self), bundle: nil), forCellReuseIdentifier: String(describing: ModuleListTableViewCell.self))
        tableView.layer.shadowOpacity = 0.5
        tableView.layer.shadowRadius = 5
        tableView.layer.shadowColor = UIColor.lightGray.cgColor
        tableView.clipsToBounds = false
        tableView.layer.masksToBounds = false

        tableView.delegate = self
        tableView.dataSource = self
    }
}

extension ModuleListViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return contents.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: ModuleListTableViewCell.self), for: indexPath) as! ModuleListTableViewCell
        let content = contents[indexPath.row]
        let isLast = contents.last == content
        cell.configure(title: content.name, subtitle: content.contentDescription, isSeperated: !isLast)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc: WebViewViewController = WebViewViewController.fromStoryboard()
        vc.URLString = contents[indexPath.row].link
        self.present(vc, animated: true)
    }
}


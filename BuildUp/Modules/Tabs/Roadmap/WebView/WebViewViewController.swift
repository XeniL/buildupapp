//
//  WebViewViewController.swift
//  BuildUp
//
//  Created by Nikita Elizarov on 04.05.2020.
//  Copyright © 2020 Nikita Elizarov. All rights reserved.
//

import UIKit
import WebKit

class WebViewViewController: UIViewController, UIScrollViewDelegate, WKUIDelegate {
    
    private struct Constants {
        static let horizontalFactor: CGFloat = 14
        static let tableViewBottomInset: CGFloat = 100
        static let emptyViewHorizontalInset: CGFloat = 20
        static let animationDuration: TimeInterval = 0.3
        static let animationDamping: CGFloat = 0.5
        static let animationVelocity: CGFloat = 0.5
        static let navigationBarOffset: CGFloat = -34.0
        static let timerInterval: TimeInterval = 0.7
    }

    @IBOutlet weak var webView: WKWebView!
    @IBOutlet weak var stackView: UIStackView!
    @IBOutlet private var completeButton: UIButton!
    @IBOutlet private var periodButton: UIButton!
    
    var URLString: String!
    var time: TimeInterval = 0
    
    private var animationTimer: Timer?
    private var timer: Timer?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        stackView.isHidden = true
        webView.scrollView.delegate = self
        
        let myURL = URL(string: URLString)
        let myRequest = URLRequest(url: myURL!)
        let timer = Timer.scheduledTimer(withTimeInterval: 1.0, repeats: true) { timer in
            self.time += 1
            let formatter = DateComponentsFormatter()
            formatter.unitsStyle = .positional
            formatter.allowedUnits = [ .minute, .second ]
            formatter.zeroFormattingBehavior = [ .pad ]
            let formattedDuration = formatter.string(from: self.time)
            DispatchQueue.main.async {
                self.stackView.isHidden = false
                self.periodButton.setTitle("Затрачено времени: \(formattedDuration ?? "")", for: .normal)
            }
        }
        
        self.timer = timer
        
        webView.load(myRequest)
    }
    
    @IBAction func complete(_ sender: UIButton) {
        self.dismiss(animated: true)
    }
    
    func showTimer(show: Bool) {
        let transform = show ? .identity : CGAffineTransform(translationX: .zero, y: Constants.tableViewBottomInset)

        UIView.animate(withDuration: Constants.animationDuration,
                       delay: .zero,
                       usingSpringWithDamping: Constants.animationDamping,
                       initialSpringVelocity: Constants.animationVelocity,
                       animations: { [weak self] in
                           self?.stackView.transform = transform
            })
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
           showTimer(show: false)
           animationTimer?.invalidate()
           animationTimer = Timer.scheduledTimer(withTimeInterval: Constants.timerInterval,
                                                 repeats: false) { [weak self] _ in
                                                    self?.showTimer(show: true)
           }
       }
}

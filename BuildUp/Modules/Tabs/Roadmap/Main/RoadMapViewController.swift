//
//  RoadMapViewController.swift
//  BuildUp
//
//  Created by Nikita Elizarov on 03.05.2020.
//  Copyright © 2020 Nikita Elizarov. All rights reserved.
//

import UIKit

class RoadMapViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    
    var stages: Road = []
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    let buildUpNetworker = BuildUpNetworking()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Roadmap"

        buildUpNetworker.loadRoadMap(userName: AppSettings.instance.userName ?? "") { result in
            switch result {
            case .success(let response):
                do {
                    let filteredResponse = try response.filterSuccessfulStatusCodes()
                    let stages = try filteredResponse.map(Road.self)
                    self.stages = stages
                    self.tableView.reloadData()
                }
                catch let error {
                    print(error.localizedDescription)
                }
            case .failure(let error):
                print(error.localizedDescription)
            }
        }
        
        tableView.register(UINib(nibName: String(describing: RoadMapItemTableViewCell.self), bundle: nil), forCellReuseIdentifier: String(describing: RoadMapItemTableViewCell.self))
        
        tableView.register(UINib(nibName: String(describing: ReverseRoadMapTableViewCell.self), bundle: nil), forCellReuseIdentifier: String(describing: ReverseRoadMapTableViewCell.self))
        
        tableView.delegate = self
        tableView.dataSource = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        applyGradientToNavBar()
    }
}

extension RoadMapViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return stages.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let index = indexPath.row + 1
        let stageNumber = index < 10 ? "0\(index)" : "\(index)"
        
        if indexPath.row == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: RoadMapItemTableViewCell.self), for: indexPath) as! RoadMapItemTableViewCell
            cell.configure(title: stages[indexPath.row].name, number: stageNumber, image: UIImage(named: "roadOrig")!)
            return cell
        } else if indexPath.row % 2 == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: RoadMapItemTableViewCell.self), for: indexPath) as! RoadMapItemTableViewCell
            cell.configure(title: stages[indexPath.row].name, number: stageNumber, image: UIImage(named: "roadContinued")!)
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: ReverseRoadMapTableViewCell.self), for: indexPath) as! ReverseRoadMapTableViewCell
            cell.configure(title: stages[indexPath.row].name, subtitle: "", number: stageNumber)
            return cell
        }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc: ModuleDetailViewController = ModuleDetailViewController.fromStoryboard()
        vc.title = stages[indexPath.row].name
        vc.contentInfo = stages[indexPath.row]
        vc.contents = stages[indexPath.row].contents
        navigationController?.pushViewController(vc, animated: true)
    }
}

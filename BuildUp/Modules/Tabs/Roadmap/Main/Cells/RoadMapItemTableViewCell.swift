//
//  RoadMapItemTableViewCell.swift
//  BuildUp
//
//  Created by Nikita Elizarov on 03.05.2020.
//  Copyright © 2020 Nikita Elizarov. All rights reserved.
//

import UIKit

class RoadMapItemTableViewCell: UITableViewCell {
    
    @IBOutlet weak var roadNumberLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subtitleLabel: UILabel!
    @IBOutlet weak var roadImage: UIImageView!

    func configure(title: String, number: String, image: UIImage) {
        roadImage.image = image
        titleLabel.text = title
        roadNumberLabel.text = number
    }
}

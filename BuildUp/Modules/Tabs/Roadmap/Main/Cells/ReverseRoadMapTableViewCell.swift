//
//  ReverseRoadMapTableViewCell.swift
//  BuildUp
//
//  Created by Nikita Elizarov on 03.05.2020.
//  Copyright © 2020 Nikita Elizarov. All rights reserved.
//

import UIKit

class ReverseRoadMapTableViewCell: UITableViewCell {

    @IBOutlet weak var roadNumberLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subtitleLabel: UILabel!

    func configure(title: String, subtitle: String, number: String) {
        titleLabel.text = title
        subtitleLabel.text = subtitle
        roadNumberLabel.text = number
    }
}

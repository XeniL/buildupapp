//
//  Road.swift
//  BuildUp
//
//  Created by Nikita Elizarov on 04.05.2020.
//  Copyright © 2020 Nikita Elizarov. All rights reserved.
//

import UIKit

// MARK: - RoadElement
struct RoadElement: Codable {
    let contents: [Content]
    let creationDate: String
    let roadDescription, id, name: String

    enum CodingKeys: String, CodingKey {
        case contents, creationDate
        case roadDescription = "description"
        case id, name
    }
}

// MARK: - Content
struct Content: Codable, Equatable {
    let contentType: String
    let creationDate: String
    let contentDescription, id, link, name: String
    let timeInSeconds: Int

    enum CodingKeys: String, CodingKey {
        case contentType, creationDate
        case contentDescription = "description"
        case id, link, name, timeInSeconds
    }
}

// MARK: - CreationDate
struct CreationDate: Codable {
    let date, day, hours, minutes: Int
    let month, nanos, seconds, time: Int
    let timezoneOffset, year: Int
}

enum ContentType: String {
    case VIDEO
    case PAPER
    case BOOK
    case COURSE
    
    var image: UIImage {
        switch self {
        case .VIDEO:
            return UIImage(named: "contentVideo")!
        case .BOOK:
            return UIImage(named: "contentBook")!
        case .COURSE:
            return UIImage(named: "contentCourse")!
        case .PAPER:
            return UIImage(named: "contentArticle")!
        }
    }
    
    var localized: String {
        switch self {
        case .VIDEO:
            return "Видео"
        case .BOOK:
            return "Книги"
        case .COURSE:
            return "Курсы"
        case .PAPER:
            return "Статьи"
        }
    }
}

typealias Road = [RoadElement]

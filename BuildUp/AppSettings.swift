//
//  AppSettings.swift
//  BuildUp
//
//  Created by Nikita Elizarov on 04.05.2020.
//  Copyright © 2020 Nikita Elizarov. All rights reserved.
//

import Foundation

struct AppSettings {
    
    static var instance = AppSettings()
    
    private init() {}
    
    static let userNameKey = "UserName"
    static let userUUID = "UUID"
    static let dailyFreeTime = "dailyFreeTime"
    static let weeklyFreeTime = "weeklyFreeTime"
    
    // User Data
    var userName: String? {
        get {
            return UserDefaults.standard.string(forKey: AppSettings.userNameKey)
        }
        set {
            UserDefaults.standard.set(newValue, forKey: AppSettings.userNameKey)
        }
    }
    
    var uuid: String? {
        get {
            return UserDefaults.standard.string(forKey: AppSettings.userUUID)
        }
        set {
            UserDefaults.standard.set(newValue, forKey: AppSettings.userUUID)
        }
    }
    
    var dailyFreeTime: Int? {
        get {
            return UserDefaults.standard.integer(forKey: AppSettings.dailyFreeTime)
        }
        set {
            UserDefaults.standard.set(newValue, forKey: AppSettings.dailyFreeTime)
        }
    }
    
    var weeklyFreeTime: Int? {
        get {
            return UserDefaults.standard.integer(forKey: AppSettings.weeklyFreeTime)
        }
        set {
            UserDefaults.standard.set(newValue, forKey: AppSettings.weeklyFreeTime)
        }
    }
    
}

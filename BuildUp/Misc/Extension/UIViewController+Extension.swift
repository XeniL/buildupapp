//
//  UIViewController+Extension.swift
//  BuildUp
//
//  Created by Nikita Elizarov on 03.05.2020.
//  Copyright © 2020 Nikita Elizarov. All rights reserved.
//

import UIKit

extension UIViewController {
    
    class func fromStoryboard<Type: UIViewController>() -> Type {
        let name = String(describing: self)
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        if let viewController = storyboard.instantiateViewController(identifier: name) as? Type {
            return viewController
        }
        
        fatalError("Failed to create viewController from storyboard \(name)")
    }

    func getImageFrom(gradientLayer:CAGradientLayer) -> UIImage? {
        var gradientImage:UIImage?
        UIGraphicsBeginImageContext(gradientLayer.frame.size)
        if let context = UIGraphicsGetCurrentContext() {
            gradientLayer.render(in: context)
            gradientImage = UIGraphicsGetImageFromCurrentImageContext()?.resizableImage(withCapInsets: UIEdgeInsets.zero, resizingMode: .stretch)
        }
        UIGraphicsEndImageContext()
        return gradientImage
    }
    
    func applyGradientToNavBar() {
        let gradient = CAGradientLayer()
        var bounds = navigationController?.navigationBar.bounds
        let topColor = #colorLiteral(red: 0, green: 0.4784313725, blue: 0.8823529412, alpha: 1)
        let bottomColor = #colorLiteral(red: 0.3529411765, green: 0.7843137255, blue: 0.9803921569, alpha: 1)
        bounds?.size.height += view.window?.windowScene?.statusBarManager?.statusBarFrame.height ?? 0
        guard let boundsRect = bounds else { return }
        gradient.frame = boundsRect
        gradient.colors = [topColor.cgColor, bottomColor.cgColor]
        gradient.locations = [ 0.0, 1.0]

        if let image = getImageFrom(gradientLayer: gradient) {
            navigationController?.navigationBar.setBackgroundImage(image, for: UIBarMetrics.default)
            let textAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
            navigationController?.navigationBar.titleTextAttributes = textAttributes
            navigationController?.navigationBar.tintColor = .white
            navigationController?.navigationBar.barTintColor = .white
        }
    }
}

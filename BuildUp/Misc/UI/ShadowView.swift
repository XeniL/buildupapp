//
//  ShadowView.swift
//  BuildUp
//
//  Created by Nikita Elizarov on 03.05.2020.
//  Copyright © 2020 Nikita Elizarov. All rights reserved.
//

import UIKit


class ShadowView: UIView {
    struct Constant {
        static let shadowX: CGFloat = 6
        static let shadowPadding: CGFloat = 7
        static let shadowHeight: CGFloat = 50
        static let shadowScaleX: CGFloat = 1
        static let shadowScaleY: CGFloat = 1
        static let animationDuration: Double = 0.2
    }
    
    // MARK: A CGFloat value that determines the corner radius for view
    @IBInspectable var cornerRadius: CGFloat {
        get {
            return layer.cornerRadius
        }
        set {
            layer.cornerRadius = newValue
        }
    }
    
    override var backgroundColor: UIColor? {
        didSet {
            layer.shadowColor = backgroundColor?.cgColor
        }
        willSet {
            layer.shadowColor = backgroundColor?.cgColor
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        layer.shadowOpacity = 1
        layer.shadowOffset = .zero
        layer.shadowRadius = 5
        layer.masksToBounds = false
    }
}

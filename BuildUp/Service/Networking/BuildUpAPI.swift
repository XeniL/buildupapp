//
//  BuildUpAPI.swift
//  BuildUp
//
//  Created by Nikita Elizarov on 04.05.2020.
//  Copyright © 2020 Nikita Elizarov. All rights reserved.
//

import Moya

enum BuildUpAPI {
    case login(String)
    case register(String, String, Int, Int)
    case loadRoadmap(String)
    case loadRoadPointProgress
}

extension BuildUpAPI: TargetType {
    public var baseURL: URL {
        return URL(string: "http://3.134.90.120:8080")!
    }
    
    public var path: String {
        switch self {
        case .loadRoadPointProgress:
            return "/point/progress"
        case .loadRoadmap:
            return "/roadMap"
        case .login:
            return "/login"
        case .register:
            return "/register"
        }
    }
    
    public var method: Moya.Method {
        switch self {
        case .loadRoadmap, .loadRoadPointProgress:
            return .get
        case .register, .login:
            return .post
        }
    }
    
    public var task: Task {
        switch self {
        case .loadRoadmap(let name):
            return .requestParameters(parameters: ["username": name], encoding: URLEncoding.default)
        case .login(let name):
            return .requestParameters(parameters: ["userName": name], encoding: JSONEncoding.default)
        case .register(let name, let uuid, let dailyFreeTime, let weeklyFreeTime):
            return .requestParameters(parameters: ["user_name": name,
                                                   "uuid": uuid,
                                                   "dailyFreeTime": dailyFreeTime,
                                                   "weeklyFreeTime": weeklyFreeTime],
                                      encoding: JSONEncoding.default)
        default:
            return .requestPlain
        }
    }
    public var sampleData: Data {
        switch self {
        case .register, .login, .loadRoadmap, .loadRoadPointProgress:
            return Data()
        }
    }

    public var headers: [String: String]? {
        return nil
    }
}

//
//  BuildUpNetworking.swift
//  BuildUp
//
//  Created by Nikita Elizarov on 04.05.2020.
//  Copyright © 2020 Nikita Elizarov. All rights reserved.
//

import Moya

struct BuildUpNetworking {
    let buildUpAPIProvider = MoyaProvider<BuildUpAPI>(plugins: [NetworkLoggerPlugin()])
        
    func loadRoadMap(userName: String, completion: @escaping (Result<Response, MoyaError>) -> Void) {
        buildUpAPIProvider.request(.loadRoadmap(userName)) { result in
            completion(result)
        }
    }
    
    func register(userName: String, UUID: String, dailyFreeTime: Int, weeklyFreeTime: Int, completion: @escaping (Result<Response, MoyaError>) -> Void) {
        buildUpAPIProvider.request(.register(userName, UUID, dailyFreeTime, weeklyFreeTime)) { result in
            completion(result)
        }
    }
    
    func login(userName: String, completion: @escaping (Result<Response, MoyaError>) -> Void) {
        buildUpAPIProvider.request(.login(userName)) { (result) in
            completion(result)
        }
    }
}
